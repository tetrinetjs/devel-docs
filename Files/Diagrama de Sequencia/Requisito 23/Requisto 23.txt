@startuml

autonumber
actor Jogador
boundary Cliente

Jogador -> Cliente: Moderador solicita finalizar o jogo.
Cliente -> Cliente: Prepara o comando: startgame < start >< iDJogador > para enviar para o servidor
Cliente -> Servidor: Envia o comando em hexadecimal.
Servidor -> Cliente : Envia mensagem para o clientes usando a resposta: endgame em formato Hexadecimal.
Cliente -> Cliente : Traduz a mensagem de Hexadecimal para String.
Cliente -> Cliente : Finaliza a partida.
Cliente -> Jogador : Notifica no batepapo que a partida est� finalizada/Despausada.

@enduml
